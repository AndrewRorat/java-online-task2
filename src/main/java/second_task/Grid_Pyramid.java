package second_task;

public class Grid_Pyramid {
    public static void main(String[] args) {
        int line = 5;
        for (int x=0; x<line+1; ++x){
            for (int j=1; j<=x; ++j){
                System.out.print((char)(j+64));
            }
            System.out.println();
        }
    }
}
