package second_task;

import java.util.Scanner;

public class MyProject {

    public static void main(String[] args) {
        final int attempts = 50;
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the number of people at the party: ");
        System.out.println("Number must be > 2");
        int n = sc.nextInt();
        int countRumor = 0;
        int peopleRumored = 0;
        for (int i = 0; i < attempts; i++) {
            boolean people[] = new boolean[n];
            people[1] = true;
            boolean alreadyRumord = false;
            int nextPerson = -1;
            int currentPerson = 1;
            while (!alreadyRumord) {
                nextPerson = 1 + (int) (Math.random() * (n - 1));
                if (nextPerson == currentPerson) {
                    while (nextPerson == currentPerson)
                        nextPerson = 1 + (int) (Math.random() * (n - 1));
                }
                if (people[nextPerson]) {
                    if (rumorSpread(people))
                        countRumor++;
                    peopleRumored = peopleRumored + countRumoredPeople(people);
                    alreadyRumord = true;
                }
                people[nextPerson] = true;
                currentPerson = nextPerson;
            }
        }

        System.out.println("Empirical probability that everyone will hear rumor except Alice in " + attempts + " attempts: " +
                (double) countRumor / attempts);
        System.out.println("Average amount of people that rumor reached is: " + peopleRumored / attempts);
    }

    public static boolean rumorSpread(boolean arr[]) {
        for (int i = 1; i < arr.length; i++)
            if (!arr[i])
                return false;
        return true;
    }

    public static int countRumoredPeople(boolean arr[]) {
        int counter = 0;
        for (int i = 1; i < arr.length; i++)
            if (arr[i])
                counter++;
        return counter;
    }
}